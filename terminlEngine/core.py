def convert(position):
    pos = [position[0] - 1, position[1] - 1]
    return pos


class Matrix:
    def __init__(self, x_size, y_size, fluid_cell, fluid_walls):
        self.fluid_cell = fluid_cell
        self.fluid_walls = fluid_walls
        self.x_size = x_size
        self.y_size = y_size
        if fluid_walls:
            cell = " " + fluid_cell + " "
        else:
            cell = fluid_cell
        self._MATRIX = [[cell for x in range(0, x_size)] for y in range(0, y_size)]
        self._UI = str()

    def get_x_size(self):
        return self.x_size

    def get_y_size(self):
        return self.y_size

    def get_fluid_cell(self):
        return self.fluid_cell

    def get_matrix(self):
        return self._MATRIX

    def make_frame_field(self):
        matrix = self._MATRIX
        frame_matrix = str()
        for y in matrix:
            for x in y:
                frame_matrix += x
            frame_matrix += "\n"
        return frame_matrix

    def make_frame_ui(self, ui_map, dict):
        ui = ui_map.split("!@!")
        for var in dict.keys():
            index = ui.index(var)
            ui[index] = dict.get(var)
        if ui[len(ui) - 1] == '':
            ui.pop()
        return ui

    def make_frame(self, ui_map, dict):
        field = self.make_frame_field()
        ui = self.make_frame_ui(ui_map, dict)
        rendered_ui = str()
        for el in ui:
            rendered_ui += str(el)
        return rendered_ui

    def change_cell(self, position, cell):
        pos = convert(position)
        if self.fluid_walls:
            self._MATRIX[pos[1]][pos[0]] = " " + cell + " "
        else:
            self._MATRIX[position[0]][position[1]] = cell
        return self._MATRIX

    def check_cell(self, position, texture_set=None):
        pos = convert(position)
        cell = self._MATRIX[pos[1]][pos[0]]
        if texture_set:
            for key, value in texture_set.items():
                if self.fluid_walls:
                    value = " " + value + " "
                if value == cell:
                    return key
        else:
            if self.fluid_walls:
                cell = " " + cell + " "# hghghghgh
        return cell
