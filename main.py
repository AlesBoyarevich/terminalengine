from terminlEngine.core import Matrix
from texture import texture

ui_map = ("score: !@!score!@!"
          "figure: !@!figure!@!"
          "!@!field!@!")

field = Matrix(10, 20, ".", True)
field.change_cell([2, 1], "#")
print(field.make_frame(ui_map, {"score": 125, "figure": 5}))
print("h")
