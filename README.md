# TerminalEngine

## Getting started

This project was made as an easy solution of make easy graphics for games and apps in terminal.

Main different of this module in place of other, this is coordinate map which very comfortable to use when you have strict reference to coordinates

- [ ] [project git](https://gitlab.com/AlesBoyarevich/terminalengine)
- [ ] [author git](https://gitlab.com/AlesBoyarevich)

```
print("hello world")
```
# Documentation
## core
### function convert(position):
    this function get a list with 2 elements: x and y coordinate.
    in core of engine if you want to get first element of first
    row you need to use [0,0] coordinate but if will be more
    comfortable to use [1,1] and this function convert this value
    in readable for core.

##### args:
- position:[x:int, y:int]

##### profitable: True

### class Matrix

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/AlesBoyarevich/terminalengine.git
git branch -M main
git push -uf origin main
```
